package cz.vse.discord.toolbox.service;

import cz.vse.discord.toolbox.configuration.DiscordBotConfiguration;
import cz.vse.discord.toolbox.discord.DiscordEventListener;
import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.presence.ClientActivity;
import discord4j.core.object.presence.ClientPresence;
import discord4j.discordjson.json.ApplicationCommandRequest;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@AllArgsConstructor
public class DiscordBotService implements CommandLineRunner {

    private final DiscordBotConfiguration configuration;

    private final List<ApplicationCommandRequest> commands;

    private final List<DiscordEventListener> listeners;

    @Override
    public void run(String... args) throws Exception {
        DiscordClient.create(configuration.token())
                .withGateway(this::start)
                .block();
    }

    private Mono<Void> start(final GatewayDiscordClient client) {
        return this.setUserStatus(client)
                .and(this.registerApplicationCommands(client))
                .and(this.registerEventListeners(client));
    }


    private Mono<Void> setUserStatus(final GatewayDiscordClient client) {
        return client.updatePresence(ClientPresence.online(ClientActivity.playing("with tools")));
    }

    private Mono<Void> registerApplicationCommands(final GatewayDiscordClient client) {
        final var rest = client.getRestClient();

        return rest.getApplicationId()
                .flatMapMany(id -> rest.getApplicationService().bulkOverwriteGlobalApplicationCommand(id, commands))
                .then();
    }

    private Mono<Void> registerEventListeners(final GatewayDiscordClient client) {
        return listeners.stream()
                .map(listener -> listener.register(client))
                .reduce(Mono::then)
                .orElseGet(Mono::empty);
    }
}
