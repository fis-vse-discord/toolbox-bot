package cz.vse.discord.toolbox.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.LocalDate;


@ConfigurationProperties(prefix = "discord")
public record DiscordBotConfiguration(String token) {
}
