package cz.vse.discord.toolbox;

import cz.vse.discord.toolbox.configuration.DiscordBotConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(DiscordBotConfiguration.class)
public class ToolboxBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToolboxBotApplication.class, args);
    }

}
