package cz.vse.discord.toolbox.discord;

import discord4j.core.GatewayDiscordClient;
import reactor.core.publisher.Mono;

public interface DiscordEventListener {

    Mono<Void> register(GatewayDiscordClient client);

}
