package cz.vse.discord.toolbox.discord.commands;

import cz.vse.discord.toolbox.discord.DiscordEventListener;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.discordjson.json.ApplicationCommandRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Component
public class CurrentWeekCommand implements DiscordEventListener {

    private final static String COMMAND_NAME = "current-week";

    private final LocalDate semesterStart;

    public CurrentWeekCommand(
            @Value("${semester-start}")
            @DateTimeFormat(pattern = "dd-MM-yyyy")
            LocalDate semesterStart
    ) {
        this.semesterStart = semesterStart;
    }

    @Bean
    public ApplicationCommandRequest command() {
        return ApplicationCommandRequest.builder()
                .name(COMMAND_NAME)
                .description("Display the current semester week")
                .defaultPermission(true)
                .build();
    }

    @Override
    public Mono<Void> register(GatewayDiscordClient client) {
        return client.on(ChatInputInteractionEvent.class)
                .filter(event -> event.getCommandName().equals(COMMAND_NAME))
                .flatMap(event -> {
                    final var week = semesterStart.until(LocalDate.now(), ChronoUnit.WEEKS) + 1;
                    final var embed = EmbedCreateSpec.builder()
                            .title("Momentálně je `" + week + ".` týden")
                            .description("Jedná se o " + (week % 2 == 0 ? "sudý" : "lichý") + " týden.")
                            .build();

                    return event.reply().withEmbeds(embed);
                })
                .then();
    }
}
